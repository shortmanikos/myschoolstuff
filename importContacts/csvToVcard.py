import csv, sys
import subprocess

# csv column numbers (actual column - 1)
col_email = 10
col_name = 2
col_sur = 1
col_home_phone = 7 
col_mobile = 6
col_mail = 10
sch_mail = 11

yaml_file_loc = "tmpDir/temp.yaml"

# read csv
with open(sys.argv[1], newline='') as csvfile:
    contact_reader = csv.reader(csvfile, dialect='excel', delimiter=';')
    next(contact_reader)
    for row in contact_reader:
        # get email
        email = row[col_mobile]
        # check whether we have a contact with this email
        check = subprocess.run(["khard", "show", email]) 
        # save a yaml file
        with open(yaml_file_loc,'w') as yaml_file:
            vcard = """
First name : {name} 
Last name : {surname}
Phone :
    home : {home_phone}
    mobile : {mobile_phone}
Email :
    home : {pers_email}
    work : {sch_mail}
Miscellaneous :
    Categories : 
{sch_cat}      
""".format(name=row[col_name], surname=row[col_sur], home_phone=row[col_home_phone], mobile_phone=row[col_mobile], pers_email=row[col_mail], sch_mail=row[sch_mail], sch_cat=categories)
            yaml_file.write(vcard)
        # if khard show "email" = 1, there is no contact with this email
        if (check.returncode == 1):
            # khard - create new contact
            subprocess.run(["khard", "new", "-i", yaml_file_loc]) 
        # if khard show "email" = 0, contact already exists
        if (check.returncode == 0):
        # khard - edit contact
            subprocess.run(["khard", "edit", "-i", yaml_file_loc, email], capture_output=True, text=True, input="y") 

