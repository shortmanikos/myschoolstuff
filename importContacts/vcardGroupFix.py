# TODO
# two way sync
# make a dictionary of email - filename and search for email in the dictionary
# to speed things up

import os
import vobject

home_dir = os.path.expanduser( '~' )
vcard_path = os.path.join(home_dir,'.contacts','epal','Επαφές')
vcards = os.listdir(vcard_path)

def is_group_vcard(vcard_obj):
    for p in vcard_obj.getChildren():
        if (p.name == 'X-ADDRESSBOOKSERVER-KIND'):
            if (p.value == 'group'):
                return True
    return False

def field_in_vcard(vcard_obj, field_name):
    for p in vcard_obj.getChildren():
        if (p.name == field_name):
            return True
    return False
            
def fix_email_format(str):
    return str[7:]

# loop through non-group vcards and map emails to vcard_files 

for vcard in vcards:
    vcard_file = open(os.path.join(vcard_path, vcard))
    vcard_obj = vobject.readOne(vcard_file)
    if is_group_vcard(vcard_obj):
        group = vcard_obj.fn.value
        for p in vcard_obj.getChildren():
            if (p.name == 'X-ADDRESSBOOKSERVER-MEMBER'):
                email = fix_email_format(p.value)
                for vcard_check in vcards:
                    vcard_file_check = open(os.path.join(vcard_path, vcard_check))
                    vcard_obj_check = vobject.readOne(vcard_file_check)
                    if (field_in_vcard(vcard_obj_check,'EMAIL') and vcard_obj_check.email.value == email):
                        if not field_in_vcard(vcard_obj_check,'CATEGORIES'):
                            vcard_obj_check.add('categories')
                            vcard_obj_check.categories.value=[group]
                        else:
                            vcard_obj_check.categories.value.append(group)
                            categories = vcard_obj_check.categories.value
                            categories = list( dict.fromkeys(categories) )
                            vcard_obj_check.categories.value = categories
                        content = vcard_obj_check.serialize()
                        vcard_file_check.close()
                        vcard_file_check = open(os.path.join(vcard_path, vcard_check),'w')
                        vcard_file_check.write(content)
                        vcard_file_check.close()
    vcard_file.close()
          
        


