#!/usr/bin/python3

import os
import vobject
import xml.etree.ElementTree as ET

def load_vcard(vcard):
         vcard_file = open(os.path.join(vcard_path, vcard))
         return vobject.readOne(vcard_file)

def is_group_vcard(vcard_obj):
    for p in vcard_obj.getChildren():
        if (p.name == 'X-ADDRESSBOOKSERVER-KIND'):
            if (p.value == 'group'):
                return True
    return False

def add_person_vcard(vcard_obj, xml_elem, mail_id_dict):
    global uid_counter
    for p in vcard_obj.getChildren():
        if (p.name == 'N'):
            first_name = p.value.given
            last_name = p.value.family
            cn_name = first_name + ' ' + last_name
        if (p.name == 'EMAIL'):
            email = p.value
    person = ET.SubElement(xml_elem,'person')
    user_id = str(uid_counter)
    person.set('uid',user_id)
    uid_counter +=1
    person.set('first-name',first_name)
    person.set('last-name',last_name)
    person.set('nickname','')
    person.set('cn',cn_name)
    addr_list = ET.SubElement(person,'address-list')
    address = ET.SubElement(addr_list,'address')
    mail_id = str(uid_counter)
    address.set('uid',mail_id)
    uid_counter += 1
    address.set('alias','')
    address.set('email',email)
    address.set('remarks','')
    ET.SubElement(person,'attribute-list')
    mail_id_dict[email] = (user_id,mail_id)
    
def add_group_vcard(vcard_obj, xml_elem, mail_id_dict):
    global uid_counter
    for p in vcard_obj.getChildren():
        if (p.name == 'FN'):
            group_name = p.value
    group = ET.SubElement(xml_elem,'group')
    group.set('uid',str(uid_counter))
    uid_counter += 1
    group.set('name',group_name)
    group.set('remarks','')
    member_list = ET.SubElement(group,'member-list')
    for p in vcard_obj.getChildren():
        if (p.name == 'X-ADDRESSBOOKSERVER-MEMBER'):
            email = p.value[7:]
            member = ET.SubElement(member_list,'member')
            member.set('pid',mail_id_dict[email][0])
            member.set('eid',mail_id_dict[email][1])
   
address_book = ET.Element('address-book')
address_book.set('name','synced')
    
home_dir = os.path.expanduser( '~' )
vcard_path = os.path.join(home_dir,'.contacts','epal','Επαφές')
vcards = os.listdir(vcard_path)
mail_id_dict = dict()
uid_counter = 737059959

for vcard in vcards:
    vcard_obj = load_vcard(vcard)
    if not is_group_vcard(vcard_obj):
        add_person_vcard(vcard_obj, address_book, mail_id_dict)

for vcard in vcards:
    vcard_obj = load_vcard(vcard)
    if is_group_vcard(vcard_obj):
        add_group_vcard(vcard_obj, address_book, mail_id_dict)

# put all emails in a folder
folder = ET.SubElement(address_book,'folder') 
folder.set('uid',str(uid_counter))
uid_counter += 1
folder.set('name','0-επαφές')
folder.set('remarks','')
item_list = ET.SubElement(folder,'item-list')
for per in mail_id_dict.values():
    per_uid = per[0]
    item = ET.SubElement(item_list,'item')
    item.set('type','person')
    item.set('uid', per_uid)
    
xml_tree = ET.ElementTree(address_book)
xml_tree.write('testme2', encoding='utf-8', xml_declaration=True, short_empty_elements=False)
