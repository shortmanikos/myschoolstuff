#!/usr/bin/python3

import os, csv, smtplib, ssl, copy, sys, time
from email.message import EmailMessage
import configparser
import fitz

def stripFunc(s):
    return s.strip()

configParser = configparser.RawConfigParser()
configParser.read("schMailer.conf")

port = configParser.get('Server', 'Port')
smtp_server = configParser.get('Server', 'Url')
username= configParser.get('Server', 'Username')
password = configParser.get('Server', 'Password')

email_dict = dict()
email_file = os.path.join('files','1.csv')
with open(email_file, 'r') as csv_file:
    email_csv = csv.reader(csv_file, delimiter=';')
    for row in email_csv:
        full_name = tuple( row[1].split(' ') + row[2].split(' ') )
        email = row[4]
        email_dict[full_name] = email


dry_run = False
        
context = ssl.create_default_context()
msg = EmailMessage()
content = """Καλημέρα,
Eπισυνάπτεται το πρόγραμμα που θα ισχύει από Δευτέρα, 6 Δεκεμβρίου 2021. Οι αλλαγές αφορούν βασικά τους τομείς Μηχανολογίας και Ηλεκτρολογίας/Ηλεκτρονικής.
Στους συναδέλφους που δεν έχουν αλλαγές σε σχέση με ότι ίσχυε πριν δεν θα αποσταλλεί email."""
# content += input("Email body:\n")
# date = input("Timetable from: (date)\n")
date = '6 Δεκεμβρίου 2021'
msg.set_content(content)
msg['Subject'] = "Πρόγραμμα από " + date
msg['From'] = "1ο ΕΠΑΛ Καλαμαριάς <1epal-kalam@sch.gr>"

myFile = open("files/2021-12-06 Πρόγραμμα ΕΠΑΛ Καλαμαριάς από 6 Δεκέμβρη.xls","rb")
myFileData = myFile.read()
msg.add_attachment(myFileData, maintype='application', subtype='vnd.ms-excel', filename='2021-12-06 Πρόγραμμα ΕΠΑΛ Καλαμαριάς από 6 Δεκέμβρη.xls')

timetable_file = os.path.join('files','1.pdf')
with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
    server.login(username, password)
    with fitz.open(timetable_file) as pdf_file:
        for page_num,page in enumerate(pdf_file):
            text = page.get_text('text').splitlines()
            file_name = text[-1][14:]
            doc_temp = fitz.open()
            doc_temp.insert_pdf(pdf_file, from_page = page_num, to_page = page_num)
            doc_temp.save(os.path.join('temp_pdf',file_name + '.pdf') )

            last_line = tuple(file_name.split(' '))
            if last_line in email_dict:
                if ('@' in email_dict[last_line]):
                    if (not dry_run):
                        myFile = open( os.path.join('temp_pdf', file_name + '.pdf') ,"rb" )
                        myFileData = myFile.read()
                        
                        msg_personalised = copy.deepcopy(msg)
                        msg_personalised['To'] = email_dict[last_line]
                        msg_personalised.add_attachment(myFileData, maintype='application', subtype='pdf', filename=file_name + '.pdf')
                        print("sending to " + file_name)
                        server.send_message(msg_personalised)
                        print("send")
                        time.sleep(2)
                else:
                    print('no valid email: ' + strt(last_line))
            else:
                print('name not in csv: ' + str(last_line))

