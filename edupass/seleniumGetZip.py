#!/usr/bin/python3                                                                                                                                                                               

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import configparser

config_parser = configparser.RawConfigParser()   
config_parser.read("pyEdupassPrep.conf")

edu_name = config_parser.get('Server', 'EduUsername')
edu_pass = config_parser.get('Server', 'EduPass')
school_code = config_parser.get('Server','SchoolCode')

chrome_options = Options()
chrome_options.add_argument("--window-size=800,600");
chrome_options.add_argument("--disable-gpu");
chrome_options.add_argument("--disable-extensions");
chrome_options.add_argument("--headless")

with webdriver.Chrome(options=chrome_options) as driver:
    print('initialising..')
    driver.implicitly_wait(20)
    driver.get("https://dilosi.services.gov.gr/dashboard/")
    print('step on me!')
    time.sleep(1)
    print('visited')
    loginButton = driver.find_element_by_class_name("MuiButton-containedSizeLarge")
    loginButton.click()
    print('login clicked')
    driver.find_element_by_id("v").send_keys(edu_name)
    driver.find_element_by_id("j_password").send_keys(edu_pass)
    time.sleep(1)
    print('input pw')
    driver.find_element_by_class_name("btn-primary").click()
    time.sleep(1)
    print('login clicked - again')
    driver.find_element_by_class_name("btn-primary").click()
    time.sleep(1)
    print('cookie accepted')
    a = driver.find_elements_by_class_name("MuiButton-label")
    a[1].click()
    time.sleep(1)
    driver.find_element_by_link_text("ΣΧΟΛΕΙΟ/"+school_code).click()
    print('clicked σχολείο')
    time.sleep(1)
    driver.find_element_by_class_name("MuiButton-containedPrimary").click()
    print('download clicked')
    time.sleep(5)
