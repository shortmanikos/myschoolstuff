#!/usr/bin/python3

# -------------------------------------------------------- #
# small script to grab the latest zip file from edupass    #
# and proccess it to create proper pdf files to record     #
# those that can enter a school building                   #
#                                                          #
# -------------------------------------------------------- #

# general imports
import argparse, os, datetime, locale, configparser, zipfile, csv
from operator import itemgetter

# email imports
import smtplib, ssl
from email.message import EmailMessage

# pdf related imports
from reportlab.lib import colors
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm, mm 
from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, PageBreak, Paragraph, Spacer
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase.pdfmetrics import registerFont, registerFontFamily
# import lib.myschoolprinter -TODO
from shutil import copy2

# selenium imports
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
import configparser

# set time to system locale - presumably el_GR.UTF8
locale.setlocale(locale.LC_TIME,'')

# get a python list of strings from a configuration file i.e. [1,2,3] -> ["1","2","3"]
def strip_func(s):
    return s.strip()
def option_to_list(group,option):
    my_option = config_parser.get(group, option)
    my_list = my_option.rstrip(']').lstrip('[').split(",")
    return list( map(strip_func,my_list) )

# add pagenumbers to a report lab generated pdf page - it presupposes that a variable named last_page is defined and equal with the total number of pages in the document
# called in doc.build( elements, onFirstPage=addPageNumber, onLaterPages=addPageNumber)
def addPageNumber(canvas, doc):
    canvas.setFont('DejaVu', 8)
    page_num = canvas.getPageNumber()
    text = "%s / %s" % (page_num,last_page)
    canvas.drawRightString(200*mm, 285*mm, text)

# get options from configuration file
config_parser = configparser.RawConfigParser()   
config_parser.read("pyEdupassPrep.conf")
# edupass stuff
edu_name = config_parser.get('Server', 'EduUsername')
edu_pass = config_parser.get('Server', 'EduPass')
school_code = config_parser.get('Server','SchoolCode')
# smtp parameters
port = config_parser.get('Server', 'Port')
smtp_server = config_parser.get('Server', 'Url')
username= config_parser.get('Server', 'Username')
password = config_parser.get('Server', 'Password')
# zip 
zip_pass = config_parser.get('Server', 'Zip_Password')
# mail recipients
to_email = config_parser.get('Mail', 'ToEmail')
cc_emails = config_parser.get('Mail', 'CcEmails')
# grades to build pdf files for
active_groups = option_to_list('Classroom-groups', 'Active_groups')
# blacklisted teacher names
blacklist = option_to_list('Pdf', 'Blacklist')

chrome_options = Options()
chrome_options.add_argument("--window-size=1440,900");
chrome_options.add_argument("--disable-gpu");
chrome_options.add_argument("--disable-extensions");
chrome_options.add_argument("--headless")

with webdriver.Chrome(options=chrome_options) as driver:
    print('initialising..')
    driver.implicitly_wait(20)
    driver.get("https://dilosi.services.gov.gr/dashboard/")
    print('step on me!')
    time.sleep(1)
    print('visited')
    loginButton = driver.find_element_by_class_name("MuiButton-containedSizeLarge")
    loginButton.click()
    print('login clicked')
    driver.find_element(By.ID, "v").send_keys(edu_name)
    driver.find_element(By.ID, "j_password").send_keys(edu_pass)
    time.sleep(1)
    print('input pw')
    driver.find_element(By.CLASS_NAME, "btn-primary").click()
    time.sleep(1)
    print('login clicked - again')
    driver.find_element(By.CLASS_NAME, "btn-primary").click()
    time.sleep(1)
    print('cookie accepted')
    a = driver.find_elements(By.CLASS_NAME, "MuiButton-label")
    a[1].click()
    time.sleep(1)
    driver.find_element(By.LINK_TEXT, "ΣΧΟΛΕΙΟ/"+school_code).click()
    print('clicked σχολείο')
    time.sleep(1)
    driver.find_element(By.CLASS_NAME, "MuiButton-containedPrimary").click()
    print('download clicked')
    time.sleep(5)

# parse arguments
parser = argparse.ArgumentParser()
# parser.add_argument("f", help="zip file name")
parser.add_argument("-m", "--mail", default=False, type=bool,
                     choices=[True, False], help="send email - defaults to False")
parser.add_argument("-c", "--copy", default=False, type=bool,
                     choices=[True, False], help="copy files - defaults to False")
args = parser.parse_args()

# unzip csv file
all_zips = filter(lambda list : list.endswith('zip'),os.listdir())
all_zip_list = list(all_zips)
all_zip_list.sort()
downloaded_file = all_zip_list[-1]

with zipfile.ZipFile(downloaded_file) as zip_file:
    zip_file.setpassword(bytes(zip_pass, 'utf-8'))
    list_of_file_names = zip_file.namelist()
    for file_name in list_of_file_names:
        if file_name.endswith('.csv'):
            zip_file.extract(file_name, 'temp_csv')
            break

# get the date from csv filename -> 
file_date_str = file_name[8:16] # ex. 20210115
file_date_date = datetime.date(int(file_date_str[0:4]), int(file_date_str[4:6]), int(file_date_str[6:8])) # temporary
file_date = file_date_date.strftime("%A, %d %B %Y") # ex. Monday, 15 January 2021 - locale dependent
file_date_short = file_date_date.strftime("%d/%m/%Y") # ex. 15/01/2021

# count red values
red_count = 0

##################################################
# --- TODO pass all prep pdf work to a module -- #
##################################################

pdfmetrics.registerFont(TTFont('DejaVu', 'DejaVuSerif.ttf'))
pdfmetrics.registerFont(TTFont('DejaVu-Bold', 'DejaVuSerif-Bold.ttf'))
pdfmetrics.registerFont(TTFont('VeraIt', 'DejaVuSerif-Italic.ttf'))
pdfmetrics.registerFont(TTFont('VeraBI', 'DejaVuSerif-BoldItalic.ttf'))
pdfmetrics.registerFont(TTFont('Vera', 'DejaVuSerif.ttf'))

registerFontFamily('DejaVu',normal='DejaVu',bold='DejaVu-Bold',italic='VeraIt',boldItalic='VeraBI')

table_style_year_date = TableStyle([('FONT', (0, 0), (-1, -1), 'DejaVu'),('FONTSIZE', (0,0), (-1,-1), 8), ('ALIGN',(0,0),(-1,-1),'RIGHT'),])


colWidths = [9*mm,19*mm,57*mm,57*mm,28*mm,20*mm]

styles = getSampleStyleSheet()

styleN = ParagraphStyle('normal style',
                        fontName='DejaVu',
                        spaceAfter=2,
                        parent=styles['Normal'],
                        )

styleJust = ParagraphStyle('normal style',
                        parent=styles['Normal'],
                        fontName='DejaVu',
                        alignment=1,
                        )


styleH = ParagraphStyle('heading1 style',
                        fontName="DejaVu-Bold",
                        parent=styles['Heading1'],
                        )
#  --- prep work END --- #

###################################
# ---     build pdf files     --- #
# - TODO -> put all styles in a - #
#           module                #
###################################

# TODO make school related text part of the config file
title_text_1 = """<font size=\"8\">
<b>1o ΗΜΕΡΗΣΙΟ ΕΠΑΛ ΚΑΛΑΜΑΡΙΑΣ</b><br />
ΚΑΡΑΜΑΝΛΗ-ΜΑΚΕΔΟΝΙΑΣ ΚΑΛΑΜΑΡΙΑ - ΤΚ 55134<br />
Τηλέφωνο: 2310 471065<br />
Email: 1epal-kalam@sch.gr<br />
</font><br />
"""

title_paragraph_begin=[]
title_paragraph_begin.append( Paragraph(title_text_1, styleJust))
title_text_2 =  [['Σχολικό έτος', '2021-2022'],['Ημ/νία',file_date_short]]
t = Table(title_text_2,rowHeights=10)
t.setStyle(table_style_year_date)
t.hAlign = 'RIGHT'
title_paragraph_begin.append( t  )

# build a pdf file for every active classroom group
for group in active_groups:
    anef = 0
    doc = SimpleDocTemplate("pdf/" + file_date_str + '_' + group + '.pdf', pagesize=A4, topMargin=10*mm, bottomMargin=12*mm, leftMargin=7*mm, rightMargin=7*mm)
    elements = []
    tmimata  = option_to_list('Classroom-groups', group)
    for tmima in tmimata:
        elements.extend(title_paragraph_begin)
        elements.append( Paragraph('<font size="12"><b>Γενικά Στοιχεία Μαθητών</b></font>', styleJust))
        text = '<font size="10"><b>Τμημα: ' + tmima + '</b><br /><b>Τάξη εγγραφής: ' + tmima[0] + '</b></font>'
        par = Paragraph(text, styleN)
        elements.append(par)
       

        elements.append(Spacer(1,2*mm))
        elements.append(Paragraph("Κατάσταση Edupass, " + file_date,styleN))
        elements.append(Spacer(1,2*mm))
        
        with open('temp_csv/' + file_name, newline='') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            data_sorted = [['Α/Α','AM','Επίθετο','Όνομα','Κατάσταση','Έλεγχος']]
            data = []
            i = 1
            for row in csv_reader:
                if (row[1]==tmima and row[5]=='ΚΟΚΚΙΝΟ'):
                    red_count += 1
                    row[5] = 'ΑΓΝΩΣΤΟ'
                # if (row[1]==tmima and row[5]!='ΠΡΑΣΙΝΟ'):
                if (row[1]==tmima):
                    data.append([i,row[4],row[2],row[3],row[5],''])
                    i+=1
            anef += len( list(filter(lambda row: row[4] == 'ΑΓΝΩΣΤΟ', data)))
            data_sorted.extend(data)

            t_style = TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),('BOX', (0,0), (-1,0), 1.25, colors.black),('BOX', (0,0), (-1,-1), 1.25, colors.black),('FONT', (0, 1), (-1, -1), 'DejaVu'), ('FONT', (0, 0), (-1, 0), 'DejaVu-Bold'), ('FONTSIZE', (0,1), (-1,-1), 10), ('ALIGN', (0, 0), (1, -1), 'CENTER') ])

            for row, values, in enumerate(data_sorted):
                for column, value in enumerate(values):
                    if ( value  == 'ΠΡΑΣΙΝΟ' ):
                      data_sorted[row][column+1] = 'OK'
                      t_style.add('BACKGROUND', (column+1, row), (column+1, row), 'LIGHTGREY' )

            t=Table(data_sorted,colWidths)
            t.setStyle(t_style)
            elements.append(t)
            elements.append(Spacer(1,5*mm))

            data = [["","Ονοματεπώνυμο","Κλάδος ΠΕ","Υπογραφή"], ["Ο καθηγητής/τρια της πρώτης ώρας "+ file_date_short + ":","","",""]]
            sign_table = Table(data,[82*mm,50*mm,40*mm,20*mm],10*mm)
            sign_table.setStyle(TableStyle([('LINEBELOW', (1,1), (3,1), 0.75, colors.black),('FONT', (0, 0), (-1,-1), 'DejaVu'),('FONTSIZE', (0,1), (0,-1), 10), ('FONTSIZE', (0,1), (0,1), 9), ]))
            elements.append(sign_table)
            elements.append(Spacer(1,2*mm))
            elements.append(Paragraph("""
                <font size="12"><b>Το παρόν δεν αποτελεί απουσιολόγιο ούτε μετέχει στην απογραφή απουσιών.</b></font><br />
                <br />
                <font size="8">Παραδίδεται στο πρώτο διάλειμα του αντίστοιχου κύκλου στην Διεύθυνση του ΕΠΑΛ Καλαμαριάς χωρίς τις βεβαιώσεις των μαθητών.</font>
                """
                ,styleN))
                    
            elements.append(PageBreak())
    print(group + ':' + str(anef))
    last_page = len(tmimata)
    doc.build( elements, onFirstPage=addPageNumber, onLaterPages=addPageNumber)

# build teacher and visitor pdf files
for latin, greek in [('episkeptes','Επισκέπτης'),('teachers','Εκπαιδευτικός')]:
    greek_s = 'Επισκεπτών' if (greek == 'Επισκέπτης') else 'Εκπαιδευτικών'
    doc = SimpleDocTemplate("pdf/" +  file_date_str + '_' + latin  + '.pdf', pagesize=A4, topMargin=10*mm, bottomMargin=12*mm, leftMargin=7*mm, rightMargin=7*mm)
    elements = []
    elements.extend(title_paragraph_begin)
    elements.append( Paragraph('<font size="12"><b>Κατάσταση ' + greek_s + '</b></font>', styleJust))
    elements.append(Spacer(1,8*mm))
    elements.append(Paragraph('<font size="12">Κατάσταση Edupass, ' + file_date + '</font>',styleJust))
    elements.append(Spacer(1,8*mm))

    with open('temp_csv/' + file_name, newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        data_sorted = [['Α/Α','Επίθετο','Όνομα','Δικαίωμα Εισόδου']]
        data = []
        i = 1
        for row in csv_reader:
            if (row[0] == greek):
                if (latin == 'teachers' and row[2] in blacklist):
                    pass
                else:
                    data.append([i,row[2],row[3],row[5]])
                    i+=1

        data.sort(key = itemgetter(1,2))
        for row, value, in enumerate(data):
            data[row][0]= row + 1
        data_sorted.extend(data)

    t_style = TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),('BOX', (0,0), (-1,0), 1.25, colors.black),('BOX', (0,0), (-1,-1), 1.25, colors.black),('FONT', (0, 1), (-1, -1), 'DejaVu'), ('FONT', (0, 0), (-1, 0), 'DejaVu-Bold'), ('FONTSIZE', (0,1), (-1,-1), 10), ('ALIGN', (0, 0), (0, -1), 'CENTER'), ('ALIGN', (1, 0), (2, -1), 'LEFT'), ('ALIGN', (3, 0), (3, -1), 'CENTER'), ('VALIGN', (0, 0), (-1, -1), 'MIDDLE') ])

    for row, values, in enumerate(data_sorted):
        for column, value in enumerate(values):
            if ( value  == 'ΠΡΑΣΙΝΟ' ):
                t_style.add('BACKGROUND', (column, row), (column, row), 'GREEN' )

    t=Table(data_sorted,[10*mm,57*mm,57*mm,50*mm],4.9*mm)
    t.setStyle(t_style)
    elements.append(t)
    elements.append(Spacer(1,5*mm))
    doc.build( elements)

# --- pdf building END --- #

if args.mail == True:
    # set message headers and body
    msg = EmailMessage()
    content = "Αυτοματοποιημένο μύνημα καταστάσεων Edupass " + file_date + ". Τη Δευτέρα απλά για δοκιμή."
    msg.set_content(content)
    msg['Subject'] = "Καταστάσεις Edupass - " + file_date_short
    msg['From'] = "1ο ΕΠΑΛ Καλαμαριάς <1epal-kalam@sch.gr>"
    msg['To'] = to_email
    msg['Cc'] = cc_emails

    # attach all pdf files found in the pdf subfolder that begin with file_date
    files = os.listdir('pdf')
    today_pdfs = filter(lambda str: str.startswith(file_date_str),files)
    for pdf_file in today_pdfs:
        if ( "teacher" not in pdf_file ):
            myFile = open("pdf/" + pdf_file ,"rb")
            myFileData = myFile.read()
            msg.add_attachment(myFileData, maintype='application', subtype='pdf', filename=pdf_file)

    #send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(username, password)
        server.send_message(msg)
else:
    print('mail flag not set')
    
if args.copy == True:
    # copy all pdf files found in the pdf subfolder that begin with file_date
    files = os.listdir('pdf')
    today_pdfs = filter(lambda str: str.startswith(file_date_str),files)
    for pdf_file in today_pdfs:
        if ( "teacher" not in pdf_file ):
            copy2("pdf/" + pdf_file, '/home/shortmanikos/Netshares/grafis/epal-kalam/Εγγραφα/2021-22/edupass/')
else:
    print('copy flag not set')

print(red_count)
